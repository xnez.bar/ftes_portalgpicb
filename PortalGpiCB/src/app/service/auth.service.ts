import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    // public  afAuth: AngularFireAuth, 
    public  router: Router
    ) { }
  
  async  login(email: string, password: string) {
    try {
      // await this.afAuth.auth.signInWithEmailAndPassword(email, password);
      // this.swal.toastMessage(1, this.successfulLoginMsg);
      this.router.navigate(['/home']);
    } catch (e) {
      // this.swal.toastMessage(3, this.notSuccessfulLoginMsg);
    }
  }
}
