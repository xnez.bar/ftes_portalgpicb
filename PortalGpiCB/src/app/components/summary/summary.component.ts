import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.css']
})
export class SummaryComponent implements OnInit {
  dataSource: any = [];
  displayedColumns: string[] = ['NUM_CUENTA', 'DSC_CUENTA', 'SALDO_CLIENTE'];


  constructor() { }

  ngOnInit() {
  }

}
