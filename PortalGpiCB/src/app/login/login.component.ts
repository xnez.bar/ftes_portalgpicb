import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../service/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private router: Router, 
    private  authService: AuthService) { }

  ngOnInit() {
  }

  login(email, password){

    this.router.navigate(['/index/summary']);

    // this.authService.login(email, password);
  }

}
