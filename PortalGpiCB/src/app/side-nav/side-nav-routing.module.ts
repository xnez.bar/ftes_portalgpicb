import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SideNavComponent } from './side-nav.component';
import { SummaryComponent } from '../components/summary/summary.component';
import { DownloadsComponent } from '../components/downloads/downloads.component';
import { QueriesComponent } from '../components/queries/queries.component';
import { PurseComponent } from '../components/purse/purse.component';

const sideRoutes: Routes = [{ 

    path: '', component: SideNavComponent, children: [
    
      /* Componente Resumen */
      {
        path: 'summary',
        component: SummaryComponent,
        // canActivate: [AuthGuardService]
      },
      /* Componente Cartera */
      {
        path: 'purse',
        component: PurseComponent,
        // canActivate: [AuthGuardService]
      },
      /* Componente Consultas */
      {
        path: 'queries',
        component: QueriesComponent,
        // canActivate: [AuthGuardService]
      },
      /* Componente Descargas */
      {
        path: 'downloads',
        component: DownloadsComponent,
        // canActivate: [AuthGuardService]
      }
      ]
    },
    {
      path: '**',
      redirectTo: 'summary'
    }
  ];
  
  @NgModule({
    imports: [RouterModule.forChild(sideRoutes)],
    exports: [RouterModule]
  })
  export class SideNavRoutingModule { }
  