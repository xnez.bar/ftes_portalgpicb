import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// import { NgxUiLoaderModule } from 'ngx-ui-loader';

/* Angular Material Declarations */
import { MatDividerModule, MatIconModule, MatSidenavModule, MatToolbarModule, MatTableModule
} from '@angular/material';
// import {MatSidenavModule} from '@angular/material/sidenav';

/* Components declarations */
import { SideNavRoutingModule } from './side-nav-routing.module';
import { SideNavComponent } from './side-nav.component';
import { SummaryComponent } from '../components/summary/summary.component';
import { DownloadsComponent } from '../components/downloads/downloads.component';
import { QueriesComponent } from '../components/queries/queries.component';
import { PurseComponent } from '../components/purse/purse.component';



@NgModule({
  declarations: [
    SideNavComponent,
    SummaryComponent,
    PurseComponent,
    DownloadsComponent,
    QueriesComponent,
  ],
  imports: [
    CommonModule,
    SideNavRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MatDividerModule,
    MatIconModule,
    MatSidenavModule,
    MatToolbarModule,
    MatTableModule
    // NgxUiLoaderModule,
    // AngularDateTimePickerModule,
    /* Angular Material Declarations */
    ],
  providers: [
  ]
})
export class SideNavModule { }
